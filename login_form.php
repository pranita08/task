<?php 

require("database.php");
//include("../../footer.php");
session_start();
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Login Beta_UMS</title>
  </head>


	<style="color: red;" id="unm-err">
<style>
body {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
    background: white;
}

.card {
    margin-bottom: 20px;
    border: none
}


.box {
    width: 500px;
    padding: 30px;
    position: absolute;
    top: 1%;
    left: 32%;
    background:  #282C35;      
    text-align: center;
    transition: 0.25s;
    margin-top: 70px
}
.box input[type="text"],
.box input[type="password"] {
    border: 0;
    background: none;
    display: block;
    margin: 20px auto;
    text-align: center;
    border: 2px solid #3498db;
    padding: 10px 10px;
    width: 250px;
    outline: none;
    color: white;
    border-radius: 24px;
    transition: 0.25s
}
.box h1 {
    color: white;
    text-transform: uppercase;
    font-weight: 500
}

.box input[type="text"]:focus,
.box input[type="password"]:focus {
    width: 300px;
    border-color: #2ecc71
}

.box input[type="submit"] {
    border: 0;
    background: none;
    display: block;
    margin: 20px auto;
    text-align: center;
    border: 2px solid #2ecc71;
    padding: 14px 40px;
    outline: none;
    color: white;
    border-radius: 24px;
    transition: 0.25s;
    cursor: pointer
}

.box input[type="submit"]:hover {
    background: #2ecc71;
}

.forgot {
    text-decoration: underline
}



</style>
<?php

//        if(isset($_REQUEST["msg"]) && $_REQUEST["msg"] != '') {
  //                      echo $_REQUEST["msg"];
    //    }

?>

</span>
<div class="full-page">
<div class="navbar">
<form action="login_action.php" method="POST" autocomplete="off" class="box">
	 <h1>Login</h1>
	<p  style="color:white;"> Please enter your login id and password!</p>
         <input type="text" name="username" id="username" placeholder="USERNAME" autofocus/>
        <div style="color:red" id="umn-err"></div>
        <br><br>
        <input type="password" name="password" id="password" placeholder="PASSWORD"/>
	<div style="color:red" id="pass-err"></div>
	<br><br>
	<a class="forgot text-muted" href="#">Forgot password?</a>
	<input type="submit" onclick="return validate();" value="Login"/>
</form>
</div>
</div>
<script>

        function validate(){
	                
                validate_form = false;

                var username = document.getElementById("username").value;
                var password = document.getElementById("password").value;

                if(username == ""){
                        document.getElementById("umn-err").innerHTML = "username can not be blank";
                         } else {
                validate_form = true;
                }
                if(password == ""){
                        document.getElementById("pass-err").innerHTML = "password can not be blank";
                } else {
                validate_form = true;
                }

                return validate_form;
        }

</script>
